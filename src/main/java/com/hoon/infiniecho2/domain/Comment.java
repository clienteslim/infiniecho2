package com.hoon.infiniecho2.domain;

import java.util.Date;

import lombok.Data;

@Data
public class Comment {
	
	private String commentId;
	private String commentCont;
	private String commentParentId;
	private String commentLv;
	private String commentSqInGroup;
	private Date commentRegTad;
	private String commentActSt;
}
