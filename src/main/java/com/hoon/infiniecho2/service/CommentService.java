package com.hoon.infiniecho2.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hoon.infiniecho2.domain.Comment;
import com.hoon.infiniecho2.repository.CommentRepository;


@Service
public class CommentService{
	
	
	private final CommentRepository repository;
	
	public CommentService(CommentRepository repository) {
		this.repository = repository;
		
	}
	
	public List<Comment> getList() {
		
		return repository.getList();
	}

	public Comment get(int commentId) {

		return repository.get(commentId);
	}

	public int add(Comment comment) {

		return repository.add(comment);
	}

	public int update(Comment comment) {

		return repository.update(comment);
	}

	public int delete(int commentId) {

		return repository.delete(commentId);
	}
	
	
}
