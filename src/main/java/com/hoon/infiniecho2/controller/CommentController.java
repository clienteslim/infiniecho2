package com.hoon.infiniecho2.controller;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hoon.infiniecho2.domain.Comment;
import com.hoon.infiniecho2.service.CommentService;

import lombok.extern.slf4j.Slf4j;


@RestController
@Slf4j
public class CommentController{
	
	
	private final CommentService service;
	
	public CommentController(CommentService service) {
		
		this.service = service;
	}
	
	@PostMapping("comment/list")
	public List<Comment> getList() {
		
				
		return service.getList();
	}
	
	@PostMapping("comment/{commentId}")
	public Comment get(@PathVariable int commentId) {

		return service.get(commentId);
	}
	
	@PostMapping("comment/add")
	public int add(Comment comment) {

		return service.add(comment);
	}
	
	
	public int update(Comment comment) {

		return service.update(comment);
	}

	@PostMapping("comment/delete/{commentId}")
	public int delete(int commentId) {

		return service.delete(commentId);
	}
	
	
}
