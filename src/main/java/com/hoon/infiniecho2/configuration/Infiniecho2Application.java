package com.hoon.infiniecho2.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.hoon.infiniecho2")
@MapperScan(basePackages = "com.hoon.infiniecho2.repository")
public class Infiniecho2Application {

	public static void main(String[] args) {
		SpringApplication.run(Infiniecho2Application.class, args);
	}
	
	
	
}
