package com.hoon.infiniecho2.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hoon.infiniecho2.domain.Comment;


@Repository
public interface CommentRepository {
	
	List<Comment> getList();
	
	Comment get(int commentId);
	
	int add(Comment comment);
	
	int update(Comment comment);
	
	int delete(int commentId);
	
}
